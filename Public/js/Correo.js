function init() {
    $("#FormCorreo").on("submit", function (e) {
        GuardarCorreo(e);
    })
}

function GuardarCorreo(e) {
    e.preventDefault(); //No se activará la acción predeterminada del evento
    var formData = new FormData($("#FormCorreo")[0]);
    $.ajax({
        url: "App/Controlador/Correo.php?op=guardaryeditar",
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            console.log(datos);
            if (datos === "1") {
                swal('Mensaje enviado te contactaremos lo mas pronto posible', {
                    icon: "success",
                });
            }
            else {
                swal(datos, {
                    icon: "warning",
                });

            }
            Limpiar();
        },
        error: function () {
            swal("Error consulte a el equipo de desarrollo", {
                icon: "warning",
            });
        }
    });
}

function Limpiar() {
    $('#Nombre').val('');
    $('#Correo').val('');
    $('#Mensaje').val('');
}


init();